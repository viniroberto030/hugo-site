FROM registry.gitlab.com/pages/hugo:latest

RUN apk update && \
    apk upgrade && \
    apk add --no-cache nodejs npm